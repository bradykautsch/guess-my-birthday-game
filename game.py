#type randint function first which makes random numbers
from random import randint
# prompt person for their name
name = input("Hi! What is your name?")
# allow computer to guess number
guess_number = 1
month_number = randint(1, 12)
year_number = randint(1924, 2004)

#make computer make its guess and ask us a question
#we have all the pieces incl. guess number, name, month, and year stored in
#four variables. Put them together to make a message.

#Guess 1

print("Guess 1 :", name, "were you born in",
      month_number, "/", year_number, "?")

#Getting a response from user
response = input("yes or no?")

#Have the computer make a decision based on user input

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

#Guess 2
guess_number = 1
month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 1 :", name, "were you born in",
      month_number, "/", year_number, "?")

response = input("yes or no?")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

#Guess 3
guess_number = 1
month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 1 :", name, "were you born in",
      month_number, "/", year_number, "?")

response = input("yes or no?")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

#Guess 4
guess_number = 1
month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 1 :", name, "were you born in",
      month_number, "/", year_number, "?")

response = input("yes or no?")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

#Guess 5
guess_number = 1
month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 1 :", name, "were you born in",
      month_number, "/", year_number, "?")

response = input("yes or no?")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("I have other things to do. Good bye.")
